# Generated by Django 4.0.6 on 2022-07-30 01:11

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('receipts', '0003_alter_account_name_alter_account_number_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='receipt',
            name='purchaser',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='receipts', to=settings.AUTH_USER_MODEL),
        ),
    ]
