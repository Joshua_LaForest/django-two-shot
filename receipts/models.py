from django.db import models

from django.contrib.auth.models import User


# Create your models here.
class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50, blank=True)
    owner = models.ForeignKey(
        User,
        related_name="categories",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )

    def __str__(self):
        return self.name


class Account(models.Model):
    name = models.CharField(max_length=100, blank=True)
    number = models.CharField(max_length=20, blank=True)
    owner = models.ForeignKey(
        User,
        related_name="accounts",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )

    def __str__(self):
        return self.name


class Receipt(models.Model):
    vendor = models.CharField(max_length=200, blank=True)
    total = models.DecimalField(decimal_places=3, max_digits=10, blank=True)
    tax = models.DecimalField(decimal_places=3, max_digits=10, blank=True)
    date = models.DateTimeField(null=True, auto_now=False)
    purchaser = models.ForeignKey(
        User, related_name="receipts", on_delete=models.CASCADE, null=True
    )
    category = models.ForeignKey(
        "ExpenseCategory",
        related_name="receipts",
        on_delete=models.CASCADE,
        blank=True,
    )
    account = models.ForeignKey(
        "Account",
        related_name="receipts",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
