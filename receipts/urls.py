from django.urls import path
from django.contrib import admin
from .views import (
    create_receipt,
    receipts_list,
    show_accounts,
    show_expense_categories,
    show_receipts,
    create_expense_category,
    create_account,
)

urlpatterns = [
    path("", receipts_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("accounts/", show_accounts, name="list_accounts"),
    path(
        "categories/",
        show_expense_categories,
        name="list_categories",
    ),
    path(
        "categories/create/",
        create_expense_category,
        name="create_category",
    ),
    path(
        "accounts/create/",
        create_account,
        name="create_account",
    ),
]
