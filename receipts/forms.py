from django import forms

from .models import Account, Receipt, ExpenseCategory


class ReceiptForm(forms.ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]


class ExpenseCategoryForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        exclude = []


class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        exclude = []
