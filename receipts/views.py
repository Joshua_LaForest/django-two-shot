from django.shortcuts import render, redirect
from .models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.
@login_required
def receipts_list(request):

    context = {"receipts": Receipt.objects.filter(purchaser=request.user)}
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    context = {}
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            # form.save(commit=False)
            form.save()
            return redirect("home")
    return render(request, "receipts/create.html", context)


@login_required
def show_receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipts,
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_expense_category(request):
    context = {}
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST or None)
        if form.is_valid():
            # form.save(commit=False)
            form.save()
            return redirect("list_categories")
    return render(request, "expense_categories/create.html", context)


@login_required
def show_expense_categories(request):
    expense_categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "expensecategory_list": expense_categories,
    }
    return render(request, "expense_categories/list.html", context)


@login_required
def create_account(request):
    context = {}
    if request.method == "POST":
        form = AccountForm(request.POST or None)
        if form.is_valid():
            # form.save(commit=False)
            form.save()
            return redirect("list_accounts")
    return render(request, "accounts/create.html", context)


# @login_required
# def show_accounts(request):
#     context = {"accounts": Account.objects.filter(owner=request.user)}
#     return render(request, "accounts/list.html", context)


@login_required
def show_accounts(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "account_list": accounts,
    }
    return render(request, "accounts/list.html", context)
